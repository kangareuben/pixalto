﻿using UnityEngine;
using System.Collections;

public class CreditsButtonScript : MonoBehaviour
{
    public GameObject player;
    public GameObject title;
    public float scrollSpeed;
    public float stopDistance;
    
    SpriteRenderer myRenderer;
    float fraction;
    float startDistance;
    Vector3 titleStartPos;
    bool rolling;

    // Use this for initialization
    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
        startDistance = Vector3.Distance(player.transform.position, this.transform.position);
        titleStartPos = title.transform.position;
        rolling = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Change color of credits button
        fraction = Vector3.Distance(player.transform.position, this.transform.position) / startDistance;
        myRenderer.color = Color.Lerp(Color.red, Color.white, fraction);

        //Credits scroll up
        if(rolling && (title.transform.position.y - titleStartPos.y < stopDistance))
        {
            float newY = title.transform.position.y + (scrollSpeed * Time.deltaTime);
            Vector3 newPosition = new Vector3(title.transform.position.x, newY, title.transform.position.z);
            title.transform.position = newPosition;
        }

        //Credits fall back down
        if(!rolling && (title.transform.position.y > titleStartPos.y))
        {
            float newY = title.transform.position.y - (scrollSpeed * 5 * Time.deltaTime);
            Vector3 newPosition = new Vector3(title.transform.position.x, newY, title.transform.position.z);
            title.transform.position = newPosition;
        }
    }

    void OnTriggerEnter2D()
    {
        rolling = true;
    }

    void OnTriggerExit2D()
    {
        rolling = false;
    }
}