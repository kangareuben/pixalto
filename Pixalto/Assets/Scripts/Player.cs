﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
	//Public variables can be set in the inspector
	public AudioClip[] audioClips = new AudioClip[2];
	public GameObject bullet;
    public Sprite[] enemyPixelSprites = new Sprite[3];
	public int initialNumPixels;
	public float invulnerabilityTimeAfterCollision;
	public Vector2 jumpForce;
    public Vector2 bounceForce;
	public float moveVelocity;
	public GameObject pixel;
    public int pixelLimit;
	public float pixelSideLength;

    //TODO: Rename these variables
	public float shakeintensity;
	public Camera maincam;
	public float vertical_compression_factor;
	public float vertical_compression_time;
	public float vertical_expansion_factor;
	public float vertical_expansion_time;
	public bool squish;
 

    //Private variables must be set in code
    //Variables which aren't specified as public are private by default
    AudioSource audioSource;
    bool canJump;
    bool canMove;
    bool canShoot;
    bool facingRight;
	bool invulnerable;
	bool jumpMonitor;
	bool horMoveplatform;

    //TODO: Rename these variables
	bool beingknockedback = false;
	float knockbacktime = 0f;

	Rigidbody2D myRigidbody;
	Transform myTransform;
	List<GameObject> pixels;

    //TODO: Rename this variable
	Vector3 originalscale;

    float pixelYRotateTo;

	//Properties
    public bool CanJump
    {
        get { return canJump; }
        set { canJump = value; }
    }

    public bool CanMove
    {
        get { return canMove; }
        set { canMove = value; }
    }
    
    public bool CanShoot
    {
        get { return canShoot; }
        set { canShoot = value; }
    }

    public bool Invulnerable
	{
		get{ return invulnerable; }
	}

    public int nPixels
    {
        get { return pixels.Count; }
    }

	// Use this for initialization
	void Start()
	{
		audioSource = GetComponent<AudioSource>();
		canJump = false;
		squish = false;
		canMove = true;
        canShoot = true;
        facingRight = true;
		invulnerable = false;
		myRigidbody = GetComponent<Rigidbody2D>();
		myTransform = GetComponent<Transform>();
		pixels = new List<GameObject>();
		originalscale = this.transform.localScale;
        pixelYRotateTo = 0f;

        //Build the player out of pixels
		BuildSelf();
	}
	
	// Update is called once per frame
	void Update()
	{
		HandleInput();
		HandleDeforms();
		MonitorJumps();
        RotatePixels();
	}

    /// <summary>
    /// Event called when a collider collides with this one
    /// </summary>
    /// <param name="collision">The collision object</param>
    void OnCollisionEnter2D(Collision2D collision)
    {
        //If the colliding object is a platform, don't move the player with a platform
        if(collision.gameObject.tag == "Platform")
        {
            transform.parent = null;
        }
        
        //If the colliding object is a moving platform, move the player with the platform
		if((collision.gameObject.tag == "HorMovingPlatform") || (collision.gameObject.tag == "VerMovingPlatform"))
        {
			if(transform.position.y > collision.gameObject.transform.position.y)
			{
				transform.parent = collision.gameObject.transform;
				if(!horMoveplatform)
                {
					originalscale.y = originalscale.y / 2f;
					horMoveplatform = true;
				}
			}
        }
    }

    /// <summary>
    /// Event called when a collider exits this one
    /// </summary>
    /// <param name="collision">The collision object</param>
	void OnCollisionExit2D(Collision2D collision)
    {
        //If the colliding object is a moving platform, the player isn't a child of it any more
		if((collision.gameObject.tag == "HorMovingPlatform") || (collision.gameObject.tag == "VerMovingPlatform"))
		{
			transform.parent = null;

			if(horMoveplatform == true)
            {
				originalscale.y = originalscale.y * 2f;
				horMoveplatform = false;
			}
		}
	}

    /// <summary>
    /// Event called when a collider trigger enters this collider
    /// </summary>
    /// <param name="collider">The other collider</param>
    void OnTriggerEnter2D(Collider2D collider)
	{
        //IF the colliding object is a bouncy platform
        if(collider.gameObject.tag == "BouncyPlatform")
        {
            //Play the bounce sound and bounce the player
            audioSource.PlayOneShot(audioClips[0]);
            myRigidbody.AddForce(bounceForce);
            canJump = false;
        }
	}

    /// <summary>
    /// Builds the player out of pixels
    /// </summary>
    void BuildSelf()
	{
		for(int i = 0; i < initialNumPixels; i++)
		{
			AddPixel();
		}
	}

	/// <summary>
	/// Adds a pixel to the player's body
	/// </summary>
	void AddPixel()
	{
        if(pixels.Count < pixelLimit)
        {
            //Instantiate the pixel object
		    GameObject p = Instantiate(pixel, this.transform.position, this.transform.rotation) as GameObject;
		
            //Set the pixel as a child of this GameObject
            p.transform.parent = myTransform;

		    //Calculate the row and column that the added pixel will be in
		    //Zero indexed at bottom left
		    int column = pixels.Count % 3;
		    int row = (int)Mathf.Floor(pixels.Count / 3);

		    //Adjust the color of the pixel based on its position on the player
		    //Closer to top right means darker
		    Color c = p.GetComponent<SpriteRenderer>().color;
            c.r = .25f - ((column + row) * .05f);
            c.g = .45f - ((column + row) * .05f);
            c.b = 1f - ((column + row) * .1f);
		    p.GetComponent<SpriteRenderer>().color = c;

		    //Adjust the position of the pixel based on the row and column calculated
		    Vector3 temp = p.transform.position;
		    temp.x += column * pixelSideLength;
		    temp.y += row * pixelSideLength;
		    p.transform.position = temp;

            //Add the pixel to the list of pixels
		    pixels.Add(p);
        }
	}

    /// <summary>
    /// Add a pixel to the player
    /// </summary>
    /// <param name="px">The pixel to add</param>
    public void AddPixel(GameObject px)
    {
        if(pixels.Count < pixelLimit)
        {
            GameObject p = Instantiate(px, transform.position, transform.rotation) as GameObject;
            p.tag = "Pixel";
            p.transform.parent = myTransform;

            //Calculate the row and column that the added pixel will be in
            //Zero indexed at bottom left
            int column = pixels.Count % 3;
            int row = (int)Mathf.Floor(pixels.Count / 3);

            //Adjust the color of the pixel based on its position on the player
            //Closer to top right means darker
            p.GetComponent<SpriteRenderer>().color = CalculatePixelTint(p, row, column);

            //Adjust the position of the pixel based on the row and column calculated
            Vector3 temp = p.transform.position;
            temp.x += column * pixelSideLength;
            temp.y += row * pixelSideLength;
            p.transform.position = temp;

            pixels.Add(p);
        }
    }

    /// <summary>
    /// Calculate the tint for a given pixel
    /// </summary>
    /// <param name="pixel">The pixel for which to calculate tint</param>
    /// <param name="row">The pixel's row</param>
    /// <param name="column">The pixel's column</param>
    /// <returns>The color to tint the given pixel</returns>
    Color CalculatePixelTint(GameObject pixel, int row, int column)
    {
        Color c = pixel.GetComponent<SpriteRenderer>().color;

        //Tint differently if the pixel originated in the player's body
        if(pixel.name == "Pixel(Clone)")
        {
            c.r = .25f - ((column + row) * .05f);
            c.g = .45f - ((column + row) * .05f);
            c.b = 1 - ((column + row) * .1f);
        }
        else
        {
            c.r = c.g = c.b = 1 - ((column + row) * .1f);
        }

        return c;
    }

	/// <summary>
	/// Removes a pixel from the player's body
	/// </summary>
	void RemovePixel()
	{
		if(pixels.Count > 0)
		{
			Destroy(pixels[pixels.Count - 1]);
			pixels.RemoveAt(pixels.Count - 1);

            //On player death, restart the game
			if(pixels.Count == 0)
			{
                Application.LoadLevel(Application.loadedLevel);
			}
		}
	}

	/// <summary>
	/// Handles all keyboard, mouse, and controller input
	/// </summary>
	void HandleInput()
	{
		//These inputs move the player right
        //TODO: Replace beingknockedback with the CanMove property
		if((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && !beingknockedback)
        {
			Move(true);
		}
		//These inputs move the player left
		else if((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && !beingknockedback)
        {
			Move (false);
		}
		else
		{
			StopMoving();
		}
		
		//These inputs cause the player to jump
		if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			if(canJump)
			{
				Jump();
			}
		}

		//This input causes the player to shoot
		if(Input.GetKeyDown(KeyCode.Space))
		{
            if(canShoot)
            {
                Shoot();
				maincam.GetComponent<CameraScript>().ShakeCamera(shakeintensity);
            }
		}

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

    /// <summary>
    /// Stretch or squish the player on jump and land
    /// </summary>
	void HandleDeforms()
	{
        //If being compressed, compress and add to compression time
		if(vertical_compression_time > 0)
        {
            float newY = this.transform.localScale.y * vertical_compression_factor;
            Vector3 newScale = new Vector3(this.transform.localScale.x, newY, this.transform.localScale.z);
            this.transform.localScale = newScale;

			vertical_compression_time -= Time.deltaTime;
		}
        //If being stretched, stretch and add to the expansion time
        else if(vertical_expansion_time > 0)
        {
            float newY = this.transform.localScale.y * vertical_expansion_factor;
            Vector3 newScale = new Vector3(this.transform.localScale.x, newY, this.transform.localScale.z);
            this.transform.localScale = newScale;

			vertical_expansion_time -= Time.deltaTime;
		}
        //Otherwise, go back to original scale
        else
        {
			RestoreScale();
		}					
	}

    /// <summary>
    /// Go back to the original player scale
    /// </summary>
	void RestoreScale()
	{
		if(this.transform.localScale.y != 1f)
        {
            float newY = (this.transform.localScale.y + originalscale.y) / 2f;
            Vector3 newScale = new Vector3(this.transform.localScale.x, newY, this.transform.localScale.z);
            this.transform.localScale = newScale;
        }
	}

    /// <summary>
    /// Monitor whether the player is jumping for stretch and squish purposes
    /// </summary>
	void MonitorJumps()
	{
		if(squish != jumpMonitor)
        {
			if(squish == true)
            {
				vertical_compression_time = 0.05f;
				vertical_expansion_time = 0.1f;
			}
            else
            {
				vertical_compression_time = 0.05f;
			}
		}

		jumpMonitor = squish;
	}

    /// <summary>
    /// Rotate pixels when the player turns
    /// </summary>
    void RotatePixels()
    {
        foreach (GameObject pixel in pixels)
        {
            float y = pixel.GetComponent<Transform>().eulerAngles.y;

            if(pixelYRotateTo < 0)
            {
                if (y > 180 || y == 0)
                {
                    if(y - 1000 * Time.deltaTime < 180 && y != 0)
                    {
                        pixel.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                    }
                    else
                    {
                        pixel.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0, y - 1000 * Time.deltaTime, 0));
                    }
                }
            }
            else if(pixelYRotateTo == 0)
            {
                if(y > 0)
                {
                    if((y - 1000 * Time.deltaTime) < 0)
                    {
                        pixel.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    }
                    else
                    {
                        pixel.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0, y - 1000 * Time.deltaTime, 0));
                    }
                }
            }
        }
    }

	/// <summary>
	/// Moves the player in the direction specified by parameter
	/// </summary>
	/// <param name="isMovingRight">If set to <c>true</c> is moving right.</param>
	void Move(bool isMovingRight)
	{
        //If changing direction
        if (facingRight != isMovingRight)
        {
            FlipPlayer();

            if(isMovingRight)
            {
                pixelYRotateTo = 0;
            }
            else
            {
                pixelYRotateTo = -1.67f;
            }
        }

        //If the player can move, move it based on the parameter
		if(canMove)
		{
			if(isMovingRight)
			{
				myRigidbody.velocity = new Vector2(moveVelocity, myRigidbody.velocity.y);
			}
			else
			{
				myRigidbody.velocity = new Vector2(-moveVelocity, myRigidbody.velocity.y);
			}
		}
	}

    /// <summary>
    /// Stops the player from moving if the keys aren't being held
    /// </summary>
	void StopMoving()
	{
		if(!beingknockedback)
        {
			myRigidbody.velocity = new Vector2(0, myRigidbody.velocity.y);
        }
		else 
		{
			knockbacktime += Time.deltaTime;

            //TODO: Expose the amount of knock back time publicly
			if(knockbacktime > 0.4f) 
			{
				knockbacktime = 0f;
				beingknockedback = false;
			}
		}
	}

    /// <summary>
    /// Flip the player's pixel tints for turn around effect
    /// </summary>
    void FlipPlayer()
    {
        facingRight = !facingRight;

        for(int i = 0; i < pixels.Count; i++)
        {
            int column = i % 3;

            if(!facingRight)
            {
                column = 2 - column;
            }

            int row = (int)Mathf.Floor(i / 3);
            Color c = CalculatePixelTint(pixels[i], row, column);
            pixels[i].GetComponent<SpriteRenderer>().color = c;
        }
    }

	/// <summary>
	/// Causes the player to jump
	/// </summary>
	void Jump()
	{
        //Play the jump sound
		audioSource.PlayOneShot(audioClips[0]);

        //Add the jump force to the player
		myRigidbody.AddForce(jumpForce);

		canJump = false;
		squish = true;
	}

    /// <summary>
    /// Shoots a pixel from the player
    /// </summary>
	void Shoot()
	{
        //Play the shoot sound
		audioSource.PlayOneShot(audioClips[1]);

        //Get the color from the last pixel in the list
		Color c = pixels[pixels.Count - 1].GetComponent<SpriteRenderer>().color;
		
        //Shoot from a position on the right of the player
        Vector3 instPos = myTransform.position;
        if(facingRight)
        {
            instPos.x += 2;
        }
        else
        {
            instPos.x -= 2;
        }
		instPos.y += 2;

        //Instantiate the bullet
		GameObject g = Instantiate(bullet, instPos, myTransform.rotation) as GameObject;
        g.GetComponent<PlayerBullet>().MovingRight = facingRight;

        //Set the sprite to the enemy pixel's sprite if necessary
        switch(pixels[pixels.Count - 1].name)
        {
            case "PlayerPixel(Clone)":
                break;
            case "EnemyPixel1(Clone)":
                g.GetComponent<SpriteRenderer>().sprite = enemyPixelSprites[0];
                break;
            case "EnemyPixel2(Clone)":
                g.GetComponent<SpriteRenderer>().sprite = enemyPixelSprites[1];
                break;
            case "EnemyPixel3(Clone)":
                g.GetComponent<SpriteRenderer>().sprite = enemyPixelSprites[2];
                break;
        }

		g.GetComponent<SpriteRenderer>().color = c;

        //Remove a pixel from the player
		RemovePixel();
	}

    /// <summary>
    /// Knocks the player back from an enemy that has damaged it
    /// </summary>
    /// <param name="center">The collision point</param>
    /// <param name="e">The enemy that has damaged the player</param>
	public void Knockback(Vector2 center, GameObject e)
	{
        Vector2 moveDirection = new Vector2(0.5f * (myRigidbody.position - center).normalized.x, 0);
		myRigidbody.MovePosition(myRigidbody.position + moveDirection);

		beingknockedback = true;

		myRigidbody.velocity = Vector2.zero;

        Vector2 newVelocity = new Vector2((myRigidbody.position - center).normalized.x * 15f, 5f);
		myRigidbody.velocity = myRigidbody.velocity + newVelocity; 
	}

    /// <summary>
    /// Start the player's Hit coroutine
    /// Called in this manner so that if an enemy is destroyed, the Hit coroutine doesn't run forever
    /// </summary>
    /// <param name="damage"></param>
    public void Damage(int damage)
    {
        StartCoroutine(Hit(damage));
    }

    /// <summary>
    /// Flash the player red after it is damaged by an enemy
    /// </summary>
    /// <param name="sprites">The player's pixels to flash</param>
    /// <param name="numTimes">The number of times to flash</param>
    /// <param name="delay">The delay between flashes</param>
    /// <param name="disable">Whether or not to disable the player's visibility altogether</param>
    IEnumerator FlashSprites(List<GameObject> sprites, int numTimes, float delay, bool disable = false)
    {
        //Number of times to flash
        for(int loop = 0; loop < numTimes; loop++)
        {
            //Cycle through all sprites
            for(int i = 0; i < sprites.Count; i++)
            {
                if(disable)
                {
                    //For disabling sprite
                    sprites[i].GetComponent<SpriteRenderer>().enabled = false;
                }
                else
                {
                    //For changing the alpha/colors
                    Color c = sprites[i].GetComponent<SpriteRenderer>().color;
                    c.r = 1f;
                    c.b -= .41f;
                    c.g -= .4f;
                    c.a = .05f;
                    sprites[i].GetComponent<SpriteRenderer>().color = c;
                }
            }

            //Delay specified amount
            yield return new WaitForSeconds(delay);

            //Cycle through all sprites
            for(int i = 0; i < sprites.Count; i++)
            {
                if(disable)
                {
                    //For disabling
                    sprites[i].GetComponent<SpriteRenderer>().enabled = true;
                }
                else
                {
                    Color c;
                    
                    //For changing the alpha/colors
                    if(sprites[i].name == "Pixel(Clone)")
                    {
                        c = sprites[i].GetComponent<SpriteRenderer>().color;
                    }
                    else
                    {
                        c = Color.white;
                    }

                    c.a = 1;
                    sprites[i].GetComponent<SpriteRenderer>().color = c;
                }
            }

            //Delay specified amount
            yield return new WaitForSeconds(delay);
        }

        //Recolor Sprites back to normal
        for (int i = 0; i < sprites.Count; i++)
        {
            int column = i % 3;

            if(!facingRight)
            {
                column = 2 - column;
            }

            int row = (int)Mathf.Floor(i / 3);
            Color c = CalculatePixelTint(pixels[i], row, column);
            sprites[i].GetComponent<SpriteRenderer>().color = c;
        }
    }

    /// <summary>
    /// Damage the player
    /// </summary>
    /// <param name="enemyDamage">The amount of damage to deal</param>
	public IEnumerator Hit(int enemyDamage)
	{
        //The player is invulnerable for a short time after being hit
        invulnerable = true;

        StartCoroutine(FlashSprites(pixels, 5, 0.01f));

        //Remove pixels equal to the amount of damage dealt
        for (int i = 0; i < enemyDamage; i++)
		{
			RemovePixel();
		}

        //Wait for invulnerability time
		yield return new WaitForSeconds(invulnerabilityTimeAfterCollision);

        //No longer invulnerable
		invulnerable = false;
	}
}