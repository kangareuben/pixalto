﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour
{
    //Public variables
	public AudioClip audioClip;
    public float lifetime;
    public float moveSpeed;
    public float rotateSpeed;

    //Private variables
	AudioSource audioSource;
    bool movingRight;

    //Properties
    public bool MovingRight
    {
        set{movingRight = value;}
    }

	//Use this for initialization
	void Start()
	{
		audioSource = GetComponent<AudioSource>();

        //Begin the death coroutine
        //This happens because the bullet has a fixed lifetime
		StartCoroutine("Death");
	}
	
	//Update is called once per frame
	void Update()
	{
        //Change position of bullet
		Vector3 temp = GetComponent<Transform>().position;
        
        if(movingRight)
        {
		    temp.x += moveSpeed * Time.deltaTime;
        }
        else
        {
            temp.x -= moveSpeed * Time.deltaTime;
        }

		GetComponent<Transform>().position = temp;

        //Change rotation of bullet
        Quaternion tempRot = GetComponent<Transform>().rotation;
        
        //This function is deprecated because it uses radians instead of degrees
        //Maybe we should change it?
        temp = tempRot.ToEulerAngles();

        if(movingRight)
        {
            temp.z -= rotateSpeed * Time.deltaTime;
        }
        else
        {
            temp.z += rotateSpeed * Time.deltaTime;
        }

        //This function is deprecated because it uses radians instead of degrees
        //Maybe we should change it?
        tempRot.SetEulerAngles(temp);

        GetComponent<Transform>().rotation = tempRot;
	}

    /// <summary>
    /// Event called when a collider collides with this one
    /// </summary>
    /// <param name="collision">The collision object</param>
	void OnCollisionEnter2D(Collision2D collision)
	{
        //Play the bullet impact sound effect
		audioSource.PlayOneShot(audioClip);

        //If colliding with a platform, destroy this bullet
		if(collision.gameObject.tag == "Platform")
		{
			Destroy(gameObject);
		}

        //If colliding with an enemy and the enemy isn't befriended, destroy the enemy and this bullet
        //Otherwise, just deatroy this bullet
		if(collision.gameObject.tag == "Enemy")
		{
			if(!collision.gameObject.GetComponent<Enemy>().Befriended)
			{
                //Destroy(collision.gameObject.gameObject);
                collision.gameObject.GetComponent<Enemy>().EnemyExplode();
			}

			Destroy(gameObject);
		}
	}

    /// <summary>
    /// Destroys this bullet after its lifetime
    /// </summary>
	IEnumerator Death()
	{
		yield return new WaitForSeconds(lifetime);
		Destroy(gameObject);
	}
}