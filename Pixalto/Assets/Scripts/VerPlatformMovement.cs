﻿using UnityEngine;
using System.Collections;

//TODO: Combine moving platform classes
public class VerPlatformMovement : MonoBehaviour
{
    //Public variables
	public float initialPosition;

    //TODO: Make these variables set in inspector, rather than by default
    //This platform begins by moving right if this value is 1, left if it is -1
    //TODO: Change this value to a boolean
	public int yDirection = 1;

	public float distanceToTraverse = 2;
	public float speed = 2f;

    //Private variables
    bool dirUp = true;

	// Use this for initialization
	void Start()
    {
		initialPosition = transform.position.y;

        if(yDirection == -1)
        {
            dirUp = false;
        }
	}
	
	// Update is called once per frame
	void Update()
    {
        //Move in the direction specified by dirUp
        if(dirUp)
        {
            transform.Translate(Vector2.up * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(-Vector2.up * speed * Time.deltaTime);
        }

        //Switch directions if reached the end of the path
		if((transform.position.y - initialPosition) >= distanceToTraverse)
        {
			dirUp = false;
		}
		
		if((transform.position.y - initialPosition) <= -distanceToTraverse)
        {
			dirUp = true;
		}
	}
}