﻿using UnityEngine;
using System.Collections;

public class EnemyPixel : MonoBehaviour
{
    //Private variables
	GameObject parentEnemy;
	GameObject player;

	//Use this for initialization
	void Start()
	{
		parentEnemy = transform.parent.gameObject;
		player = GameObject.Find("Player");
	}
	
	//Update is called once per frame
	void Update()
	{
        //If this pixel has been detached (its parent enemy has exploded), scale it down
        if(parentEnemy.GetComponent<Enemy>() != null)
        {
            if(parentEnemy.GetComponent<Enemy>().exploded)
            {
                if(gameObject.GetComponent<Transform>().localScale.x > 0)
                {
                    gameObject.GetComponent<Transform>().localScale -= new Vector3(Time.deltaTime * 5, Time.deltaTime * 5, 0);
                }
            }
        }
	}

    /// <summary>
    /// Event called when this GameObject collides with another one
    /// </summary>
    /// <param name="collision">The collision object</param>
	void OnCollisionEnter2D(Collision2D collision)
	{
        //If the colliding object is the player
		if(collision.gameObject.tag == "Player")
		{
            //If the parent enemy hasn't exploded yet
            if(!parentEnemy.GetComponent<Enemy>().exploded)
            {
                //Unless the enemy is befriended or the player is invulnerable, hit the player with this enemy's damage
			    if(!parentEnemy.GetComponent<Enemy>().Befriended && !player.GetComponent<Player>().Invulnerable)
			    {
                    //Damage the player
				    player.GetComponent<Player>().Damage(parentEnemy.GetComponent<Enemy>().Damage);
                    
                    //Knock the player back from the enemy
					player.GetComponent<Player>().Knockback((new Vector2(this.transform.position.x,this.transform.position.y)), parentEnemy);

                    //Befriending can't happen when player is taking damage
                    StopCoroutine(parentEnemy.GetComponent<Enemy>().Befriend());
                    parentEnemy.GetComponent<Enemy>().BeingBefriended = false;
                    parentEnemy.GetComponent<Enemy>().ResetColor();
			    }
            }
		}
	}

    void OnCollisionStay2D(Collision2D collision)
    {
        //If the colliding object is the player
        if (collision.gameObject.tag == "Player")
        {
            if (!parentEnemy.GetComponent<Enemy>().Befriended && !player.GetComponent<Player>().Invulnerable)
            {
                //Befriending can't happen when player is taking damage
                StopCoroutine(parentEnemy.GetComponent<Enemy>().Befriend());
                parentEnemy.GetComponent<Enemy>().BeingBefriended = false;
                parentEnemy.GetComponent<Enemy>().ResetColor();
            }
        }
    }
}