﻿using UnityEngine;
using System.Collections;

//TODO: Combine moving platform scripts
public class HorPlatformMovement : MonoBehaviour
{
    //Private variables
    //TODO: Rename this variable
	bool dirRight = true;

    //Public variables
    //TODO: Make these variables set in inspector, rather than by default
    public float distanceToTraverse = 2;
    public float initialPosition;
    public float speed = 2f;

    //This platform begins by moving right if this value is 1, left if it is -1
    //TODO: Change this value to a boolean
    public int xDirection = 1;

	// Use this for initialization
	void Start()
    {
		initialPosition = transform.position.x;
        
        if(xDirection == -1)
        {
            dirRight = false;
        }
	}
	
	// Update is called once per frame
	void Update()
    {
        //Move in the direction specified by dirRight
        if(dirRight)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(-Vector2.right * speed * Time.deltaTime);
        }

        //Switch directions if reached the end of the path
		if((transform.position.x - initialPosition) >= distanceToTraverse)
        {
			dirRight = false;
		}
		
		if((transform.position.x - initialPosition) <= -distanceToTraverse)
        {
			dirRight = true;
		}
	}
}