﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
	//Private variables
    Vector3 startPos;
	Vector3 leftPos;
	Vector3 rightPos;
	Vector3 shakeStartPos;
	float startTime;
	float fraction;
    GameObject player;
    Vector3 originalOffset;
	
    
    [SerializeField]
    float rightOffset;
    [SerializeField]
	float leftOffset;
    [SerializeField]
	float speed;
    [SerializeField]
    float shakeDuration;
    [SerializeField]
    float shakeAmount;
    [SerializeField]
    float decreaseFactor;

	// Use this for initialization
	void Start() 
	{
        originalOffset = new Vector3(0, 2, -10);
        player = GameObject.Find("Player");
        shakeStartPos = this.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update() 
	{
		UpdatePositions();

        //Start movement if the player moves to the side
		if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            startTime = Time.time;
        }

        //Move right if the player is moving to the right
		if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
			fraction = ((Time.time - startTime) * speed) / Vector3.Distance(startPos, rightPos);
			this.transform.position = Vector3.Lerp(this.transform.position, rightPos, fraction);
		}

        //Move left if the player is moving to the left
		if(Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow))
        {
			fraction = ((Time.time - startTime) * speed) / Vector3.Distance(startPos, leftPos);
			this.transform.position = Vector3.Lerp(this.transform.position, leftPos, fraction);
		} 

		//Shake the camera if the player has fired
		if(shakeDuration > 0)
		{
			this.transform.localPosition = shakeStartPos + Random.insideUnitSphere * shakeAmount;

			shakeDuration -= Time.deltaTime;
			if(shakeDuration <= 0) 
			{
				shakeDuration = 0f;
				this.transform.localPosition = shakeStartPos;
			}
		}
	}

    //Update offset positions to match the player's movement
	void UpdatePositions()
	{
		startPos = player.transform.position + originalOffset;
		leftPos = startPos + new Vector3(-leftOffset, 0, 0);
		rightPos = startPos + new Vector3(rightOffset, 0, 0);
	}

    //Shake the camera if the player has fired
	public void ShakeCamera(float duration)
	{
		shakeDuration = duration;
		shakeStartPos = this.transform.localPosition;
	}

	public void ResetCameraOffset()
	{
		//this.transform.position = startPos;
		this.transform.localPosition = originalOffset;
	}
}