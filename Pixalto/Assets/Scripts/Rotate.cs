﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
    [SerializeField]
    float speed;

    Transform myTransform;

	// Use this for initialization
	void Start()
    {
        myTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update()
    {
        //Change rotation of checkpoint
        Quaternion tempRot = myTransform.rotation;

        //This function is deprecated because it uses radians instead of degrees
        //Maybe we should change it?
        Vector3 temp = tempRot.ToEulerAngles();

        temp.z -= speed * Time.deltaTime;

        //This function is deprecated because it uses radians instead of degrees
        //Maybe we should change it?
        tempRot.SetEulerAngles(temp);

        myTransform.rotation = tempRot;
	}
}