﻿using UnityEngine;
using System.Collections;

public class PlaybuttonScript : MonoBehaviour
{
	GameObject player;
    SpriteRenderer myRenderer;
	float fraction;
	float startDistance;

	// Use this for initialization
	void Start() 
	{
        player = GameObject.Find("Player");
		myRenderer = GetComponent<SpriteRenderer>();
		startDistance = Vector3.Distance(player.transform.position, this.transform.position);
	}
	
	// Update is called once per frame
	void Update() 
	{
		fraction = Vector3.Distance(player.transform.position, this.transform.position) / startDistance;
		myRenderer.color = Color.Lerp(Color.red, Color.white, fraction);
	}

	void OnTriggerEnter2D()
	{
		Application.LoadLevel("GameScene");
	}
}